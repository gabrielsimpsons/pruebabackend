package co.gabriel.sierra.backendgabriel.service.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Gabriel Sierra - gsierrar@gmail.com
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(co.gabriel.sierra.backendgabriel.service.ClientFacadeREST.class);
        resources.add(co.gabriel.sierra.backendgabriel.service.ProductFacadeREST.class);
        resources.add(co.gabriel.sierra.backendgabriel.service.ProductStockFacadeREST.class);
        resources.add(co.gabriel.sierra.backendgabriel.service.StoreFacadeREST.class);
    }

}
