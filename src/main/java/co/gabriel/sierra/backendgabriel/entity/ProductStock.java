package co.gabriel.sierra.backendgabriel.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gabriel Sierra - gsierrar@gmail.com
 */
@Entity
@Table(name = "product_stock")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductStock.findAll", query = "SELECT p FROM ProductStock p")
    , @NamedQuery(name = "ProductStock.findByIdProductStock", query = "SELECT p FROM ProductStock p WHERE p.idProductStock = :idProductStock")
    , @NamedQuery(name = "ProductStock.findByQuantity", query = "SELECT p FROM ProductStock p WHERE p.quantity = :quantity")})
public class ProductStock implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_product_stock")
    private Integer idProductStock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private int quantity;
    @JoinColumn(name = "id_store", referencedColumnName = "id_store")
    @ManyToOne(optional = false)
    private Store idStore;
    @JoinColumn(name = "id_product", referencedColumnName = "id_product")
    @ManyToOne(optional = false)
    private Product idProduct;

    public ProductStock() {
    }

    public ProductStock(Integer idProductStock) {
        this.idProductStock = idProductStock;
    }

    public ProductStock(Integer idProductStock, int quantity) {
        this.idProductStock = idProductStock;
        this.quantity = quantity;
    }

    public Integer getIdProductStock() {
        return idProductStock;
    }

    public void setIdProductStock(Integer idProductStock) {
        this.idProductStock = idProductStock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Store getIdStore() {
        return idStore;
    }

    public void setIdStore(Store idStore) {
        this.idStore = idStore;
    }

    public Product getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Product idProduct) {
        this.idProduct = idProduct;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProductStock != null ? idProductStock.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductStock)) {
            return false;
        }
        ProductStock other = (ProductStock) object;
        if ((this.idProductStock == null && other.idProductStock != null) || (this.idProductStock != null && !this.idProductStock.equals(other.idProductStock))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gabriel.sierra.backendgabriel.ProductStock[ idProductStock=" + idProductStock + " ]";
    }

}
